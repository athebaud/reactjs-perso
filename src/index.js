import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Covid19 from './Covd19'

ReactDOM.render(
  <React.StrictMode>
    <Covid19 />
  </React.StrictMode>,
  document.getElementById('root')
);