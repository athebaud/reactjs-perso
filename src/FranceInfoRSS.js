import React from 'react';
import axios from 'axios';

function ArticleDeFlux(props) {
  return (
    <li>
      <p><a href={props.lien} target="_blank" rel="noopener noreferrer">{props.titre}</a>
      <small> {props.resume}</small></p>
    </li>
  )
}

function ListeDArticles(props) {

  let listeDArticlesAsJSX = [];
  let domParser = new DOMParser();
  let fluxAsDOM = domParser.parseFromString(props.flux, 'text/xml');
  let fluxItems = fluxAsDOM.querySelectorAll('item');
  let listLength = fluxItems.length > 10 ? 10 : fluxItems.length;
  for (let i = 0; i < listLength; i++) {
    let item = fluxItems[i];
    listeDArticlesAsJSX.push(
      <ArticleDeFlux
        key={i}
        titre={item.querySelector('title').textContent}
        resume={item.querySelector('description').textContent.replace(/&nbsp;/gi, ' ')}
        lien={item.querySelector('link').textContent}
      />
    )
  }

  return (
    <ul>{listeDArticlesAsJSX}</ul>
  )
}

class FranceInfoRSS extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      flux: [],
    }
  }

  componentDidMount() {
    axios.get('/titres.rss')
      .then(response => {
        return response.data;
      })
      .then(text => {
        return this.setState({ flux: text });
      })
      .catch(e => {
        console.error('Erreur lors de la requête RSS');
      });
  }

  render() {
    return (
      <div className="module rss-module">
        <h2>Derniers titres France Info</h2>
        <ListeDArticles flux={this.state.flux} />
      </div>
    )
  }

}

export default FranceInfoRSS
