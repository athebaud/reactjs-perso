import React from 'react';

class RonSwanson extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reponse: '',
        }
    }

    componentDidMount() {
        fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
            .then(response => { return response.json(); })
            .then(data => {
                this.setState({ reponse: data[0] });
            })
            .catch(e => {
                console.log('Erreur lors de la requête Ron');
            });
    }

    render() {
        return (
            <div className="module">
                <p>
                    <q>{this.state.reponse}</q>
                    <br />&ndash; Ron Swanson
        </p>
            </div>
        )
    }
}

export default RonSwanson