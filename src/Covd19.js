import React from 'react'
import * as d3 from "d3";
import Chart from 'react-google-charts'

function SelecteurDeDate(props) {

    const options = { weekday:'long', year: 'numeric', month: 'long', day: 'numeric' };

    let dates = props.jours.map((element, index) => {
        return <option key={index} value={index} selected={index === props.jourJ}>{new Date(element.jour).toLocaleDateString(undefined, options)}</option>
    })

    return (
        <div>
            <label htmlFor="select-date">Autre date : </label>
            <select name="date" id="select-date" onChange={props.gestionChangement} >{dates}</select>
        </div>
    )
}

function CompteRendu(props) {
    if (props.jourSelectionne === -1) {
        return <div>En attente des chiffres...</div>
    } else {
        const options = { weekday:'long', year: 'numeric', month: 'long', day: 'numeric' };
        return (
            <div>
                <h2>Au {new Date(props.jours[props.jourSelectionne].jour).toLocaleDateString(undefined, options)} :</h2>
                <p>Nombre de personnes actuellement hospitalisées :  <strong>{props.jours[props.jourSelectionne].hosp}</strong></p>
                <p>Nombre de personnes actuellement en réanimation ou soins intensifs : <strong>{props.jours[props.jourSelectionne].rea}</strong></p>
                <p>Nombre cumulé de personnes retournées à domicile : <strong>{props.jours[props.jourSelectionne].rad}</strong></p>
                <p>Nombre cumulé de personnes décédées à l'hôpital	: <strong>{props.jours[props.jourSelectionne].dc}</strong></p>
            </div>
        );
    }
}

function GraphiqueLignes(props) {
    const options = {
        title: "Évolution des données hospitalières dans le temps",
        curveType: "function",
        legend: { position: "bottom" }
      };

    const dateOptions = { month: 'numeric', day: 'numeric' };

    let data = [
        [ "Jour", "Hospitalisations", "Réanimations", "Rétablissments", "Décès" ]
    ];

    props.jours.forEach(jour => {
        data.push([new Date(jour.jour).toLocaleDateString(undefined, dateOptions), jour.hosp, jour.rea, jour.rad, jour.dc]);
    })

    return (
        <div>
            <Chart
                chartType="LineChart"
                width="100%"
                height="500px"
                options={options}
                data={data}
                loader={<div>Chargement du graphique</div>}
            />
        </div>
    )
}

function GraphiqueColonnes(props) {
    
    let critere;
    switch (props.critere) {
        case 'hosp' : 
            critere = "hospitalisations";
            break;
        case 'rea' :
            critere = "réanimations"
            break;
        case 'rad' :
            critere = "rétablissements"
            break;
        case 'dc' :
            critere = "décès à l'hôpital"
            break;
        default :
            critere = "";
    }

    const options = {
        title: "Évolution journalière du nombre de " + critere,
        legend: { position: "bottom" }
    };

    let data = [
        ["Data", "Nombre de " + critere]
    ];

    const dateOptions = { month: 'numeric', day: 'numeric' };

    let jourPrec = 0;
    let premierJour = true;
    props.jours.forEach(jour => {
        if (premierJour) {
            premierJour = false;
        } else {
            let diff = jour.dc - jourPrec;
            data.push([new Date(jour.jour).toLocaleDateString(undefined, dateOptions), diff]);
        }
        jourPrec = jour.dc;

    })

    return (
        <div>
            <Chart
                chartType="ColumnChart"
                width="100%"
                height="500px"
                options={options}
                data={data}
                loader={<div>Chargement du graphique</div>}
            />
        </div>
    )
}

class Covid19 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            indexDuJourSelectionne: -1,
            jours: [],
        }
        this.gestionChangementDeDate = this.gestionChangementDeDate.bind(this);
    }

    gestionChangementDeDate = (event) => {
        let selecteur = document.getElementById("select-date");
        if (selecteur != null) {
            this.setState({indexDuJourSelectionne: event.target.value,});
        }
    }

    componentDidMount() {
        d3.dsv(';', 'https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7')
        .then(data => {
            let jour = data[0].jour;
            let h = 0, r = 0, g = 0, m = 0;
            data.forEach(element => {
                let jour2 = element.jour;
                if (jour2 !== jour) {
                    this.setState(prev_state => (
                        { jours:
                            [...prev_state.jours,
                                {
                                    jour: jour,
                                    hosp: h,
                                    rea: r,
                                    rad: g,
                                    dc: m,
                                }
                            ],
                        }
                    ));
                    jour = jour2;
                    h = 0;
                    r = 0;
                    g = 0;
                    m = 0;
                }
                if (element.sexe === '0') {
                    h += parseInt(element.hosp, 10);
                    r += parseInt(element.rea, 10);
                    g += parseInt(element.rad, 10);
                    m += parseInt(element.dc, 10);
                }
            });
            this.setState(prev_state => (
                { jours:
                    [...prev_state.jours,
                        {
                            jour: jour,
                            hosp: h,
                            rea: r,
                            rad: g,
                            dc: m,
                        }
                    ],
                }
            ));
            let index = this.state.jours.length - 1;
            this.setState({
                indexDuJourSelectionne: index,
            })
        })
        .catch(e => {
            console.log('Erreur lors de la requête Covid-19');
        });
    }

    render() {
        return (
            <div>
                <h1 className="text-info">Données hospitalières relatives à l'épidémie de COVID-19 en France</h1>
                <div className="covid-module">
                    <CompteRendu
                        jours={this.state.jours}
                        jourSelectionne={this.state.indexDuJourSelectionne}
                    />
                    <SelecteurDeDate
                        jours={this.state.jours}
                        jourJ={this.state.indexDuJourSelectionne}
                        gestionChangement={this.gestionChangementDeDate}
                    />
                </div>
                <div className="covid-modules-group">
                    <div className="covid-module">
                        <GraphiqueLignes jours={this.state.jours} />
                    </div>
                    <div className="covid-module">
                        <GraphiqueColonnes jours={this.state.jours} critere="dc" />
                    </div>
                </div>
                <p className="text-info">
                    <a target="_blank" rel="noopener noreferrer" href="https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/">
                    <small>Source : Santé publique France</small>
                    </a>
                </p>
            </div>
        )
    }
}

export default Covid19